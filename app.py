from flask import Flask
from flask import request
# from flask import Response

import json
import requests
import os

import sys
import signal
import time

import pandas as pd
import numpy as np

import pickle

from sklearn.cluster import KMeans
from sklearn.neighbors import NearestNeighbors
import time
import gc

import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()

import tensorflow_hub as hub
import sentencepiece as spm


# версия используемого индекса
with open('env', 'r') as f:
    idx = int(f.readline())

# подгрузим модель для сервинга эмбеддингов
module = hub.Module("https://tfhub.dev/google/universal-sentence-encoder-lite/2")

input_placeholder = tf.sparse_placeholder(tf.int64, shape=[None, None])
encodings = module(
    inputs=dict(
        values=input_placeholder.values,
        indices=input_placeholder.indices,
        dense_shape=input_placeholder.dense_shape))

with tf.Session() as sess:
    spm_path = sess.run(module(signature="spm_path"))

sp = spm.SentencePieceProcessor()
with tf.io.gfile.GFile(spm_path, mode="rb") as f:
    sp.LoadFromSerializedProto(f.read())
# print("SentencePiece model loaded at {}.".format(spm_path))

def process_to_IDs_in_sparse_format(sp, sentences):
    # An utility method that processes sentences with the sentence piece processor
    # 'sp' and returns the results in tf.SparseTensor-similar format:
    # (values, indices, dense_shape)
    ids = [sp.EncodeAsIds(x) for x in sentences]
    max_len = max(len(x) for x in ids)
    dense_shape=(len(ids), max_len)
    values=[item for sublist in ids for item in sublist]
    indices=[[row,col] for row in range(len(ids)) for col in range(len(ids[row]))]
    return (values, indices, dense_shape)

app = Flask(__name__)

@app.route("/")
def hello():
    time.sleep(1)
    return 'hi there!'

@app.route("/get_similar_questions")
def similarity():
    init_question = str(request.args['question'])
#     time.sleep(1)
    
#     url = "http://65.108.214.117:8501/v1/models/use:predict"

    # получаем запрос
#     response = requests.request("POST", url, json={"instances":[init_question]}).json()['predictions']

    response = [init_question]
    
    # Compute a representation for each message, showing various lengths supported.
    

    values, indices, dense_shape = process_to_IDs_in_sparse_format(sp, response)

    # Reduce logging output.
    # logging.set_verbosity(logging.ERROR)

    with tf.Session() as session:
        session.run([tf.global_variables_initializer(), tf.tables_initializer()])
        response_embeddings = session.run(
            encodings,
            feed_dict={input_placeholder.values: values,
                    input_placeholder.indices: indices,
                    input_placeholder.dense_shape: dense_shape})
    
    
#     time.sleep(1)
    
    # грузим модель для определения кластера в соответствющем индексе
    km = pickle.load(open(f'/projects/data/index_{idx}/model_index_{idx}.pkl', 'rb'))
    
#     print(km)
    
    # немного преобразуем запрос
#     response = np.array(response,dtype='float32').reshape(1, 512)
    
    # определяем кластер
    cluster = str(km.predict(response_embeddings)[0])

    # грузим эмбеддинги соответствующего кластера
    embeddings = pickle.load(open(f'/projects/data/index_{idx}/cluster_{cluster}/embeddings.pkl', 'rb'))
#     print(embeddings)

    # ищем 10 наиболее похожих вопросов
    knn = NearestNeighbors(n_neighbors=10)
    knn.fit(embeddings)
    _, idxs = knn.kneighbors(response_embeddings)
    del embeddings
    gc.collect()
    
    questions = pickle.load(open(f'/projects/data/index_{idx}/cluster_{cluster}/questions.pkl', 'rb'))
#     print(questions)
    
    result = [f for f in questions[idxs][0]]
    
#     return '111'

    return {'similar_questions':result}
# #     return {'similar_questions':cluster}

def handler(sig, frame):
    time.sleep(1)
    print(f'YOU CALLED ME WITH {sig}')
    sys.exit(0)
    
signal.signal(signal.SIGTERM, handler)
signal.signal(signal.SIGINT, handler)

if __name__ == "__main__":
    
    app.run(host="0.0.0.0", port=4200, threaded=False)
    
    